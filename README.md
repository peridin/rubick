# Projet Rubick

## Emulation CPC6128

Avec Mame. Documentation : http://www.cpcwiki.eu/index.php/MAME 

Demarrer l'emulateur avec prise en compte de l'image disque :

```shell
$ mame cpc6128 -window -natural -flop1 rubick.dsk
```

Chargement et lancement du programme dans l'emulateur :

```
run"rubick
```

![demarrage](./static/demarrage.png)

Apres quelques manipulations :

![utilisation](./static/utilisation.png)

## Mode d'emploi d'origine du jeu

```
          ----  M O D E   D ' E M F L O I  ----

	RUBICK est une reproduction de ce jeu celebre sur ordinateur.L'avantage est 
	que 1'ordinateur vous permet de voir toutes les vues d'un seul coup; 
	de plus il vous donne les derniers coups joues ainsi qu'une vue en 
	3 dimensions.

	Tous les mouvements et les formu1es se font a partir de la vue de face. 
	Cette   vue correspond a 1'intersection des six faces en vue plane et a 
	la vue de gauche de la vue en trois dimensions.

	Vous pouvez le melanger automatiquement ou manue11ement.

	Les mouvements notes par des lettres (telles que: A+,A-,B+,B-,...) vous 
	sont donnes avec les touches correspondantes.

	Bien sur ce jeu necessite un certain temps d'adaptation (pour 1es mouvements
	et les formu1es notamment),mais n'ayez crainte,ce n'est pas la mer a boire.

    Alors les nostalgiques du rubik's,a vos c1aviers...
    
    ![rubick-8](/uploads/5706f647a50c3af004e0922a4a4ee188/rubick-8.png)
```