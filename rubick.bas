5 ON BREAK GOSUB 7
6 GOTO 10
7 RUN
10 '###########################################
20 '########## R U B I C K ####################
30 '############## par LE THIEIS Yann #########
35 '############## ecrit vers 1984-1985 #######
40 '###########################################
50 MODE 1
60 INK 0,26:INK 1,0:INK 2,2:INK 3,15:BORDER 13
70 l1=5:l2=16:l3=5:l4=9
72 ti$="MONITEUR":t=2:DIM fe$(t)
74 fe$(1)=" Couleur    "
76 fe$(2)=" Monochrome "
78 mo=2:PAPER #2,0:GOSUB 4000
80 IF j-2=1 THEN 100
90 IF j-2=2 THEN 110
100 INK 2,6:INK 3,18:INK 4,24:INK 5,15:INK 6,26:GOTO 120
110 INK 2,4:INK 3,9:INK 4,15:INK 5,20:INK 6,26
120 GOSUB 2660
129 l1=8:l2=22:l3=15:l4=19
130 ti$="M E N U":t=2:DIM fe$(t)
140 fe$(1)=" Mode d'emploi "
150 fe$(2)=" Le jeu        "
160 mo=2:PAPER #2,0:GOSUB 4000
165 BORDER 26
170 ON j-2 GOSUB 3250,370:MODE 1:INK 1,0:GOTO 120
190 STOP
370 '#########################
380 '######### CADRE #########
390 '#########################
400 INK 10,1
410 MODE 0:INK 0,26
420 DIM g(9):DIM t(12)
530 'rubick
540 GOSUB 2460
550 DIM a(9):DIM b(9):DIM c(9):DIM d(9):DIM e(9):DIM f(9)
560 FOR i=1 TO 9
570 a(i)=10:b(i)=4:c(i)=2:d(i)=5:e(i)=3:f(i)=6
580 NEXT
590 GOSUB 2880
600 ' -------- choix modes
610 l1=2:l2=10:l3=18:l4=24
615 INK 11,13:PAPER #2,11
620 ti$="E-initial":t=4:DIM fe$(t)
630 fe$(1)=" Gauche  "
640 fe$(2)=" Haut    "
650 fe$(3)=" Coups   "
660 fe$(4)=" Melange "
670 mo=4:GOSUB 4000
680 ON j-2 GOSUB 710,800,1580,3010
690 GOTO 610
710 ' -------- rubick a Gauche
720 FOR i=1 TO 9
730 g(i)=a(i):a(i)=d(i):d(i)=b(i):b(i)=c(i):c(i)=g(i):NEXT
740 FOR i=1 TO 9:g(i)=e(i):NEXT
750 e(1)=g(3):e(2)=g(6):e(3)=g(9):e(4)=g(2):e(6)=g(8):e(7)=g(1):e(8)=g(4):e(9)=g(7)
760 FOR i=1 TO 9:g(i)=f(i):NEXT
770 f(1)=g(7):f(2)=g(4):f(3)=g(1):f(4)=g(8):f(6)=g(2):f(7)=g(9):f(8)=g(6):f(9)=g(3)
780 GOSUB 2880
790 RETURN
800 ' -------- rubick vers le Haut
810 FOR i=1 TO 9
820 g(i)=a(i):a(i)=e(i):NEXT
830 e(1)=b(9):e(2)=b(8):e(3)=b(7):e(4)=b(6):e(5)=b(5):e(6)=b(4):e(7)=b(3):e(8)=b(2):e(9)=b(1)
840 b(1)=f(9):b(2)=f(8):b(3)=f(7):b(4)=f(6):b(5)=f(5):b(6)=f(4):b(7)=f(3):b(8)=f(2):b(9)=f(1)
850 FOR i=1 TO 9
860 f(i)=g(i)
870 NEXT
880 FOR i=1 TO 9:g(i)=d(i):NEXT
890 d(1)=g(7):d(2)=g(4):d(3)=g(1):d(4)=g(8):d(6)=g(2):d(7)=g(9):d(8)=g(6):d(9)=g(3)
900 FOR i=1 TO 9:g(i)=c(i):NEXT
910 c(1)=g(3):c(2)=g(6):c(3)=g(9):c(4)=g(2):c(6)=g(8):c(7)=g(1):c(8)=g(4):c(9)=g(7)
920 GOSUB 2880
930 RETURN
1580           ' C O U P S
1585 WINDOW #1,1,12,16,25:CLS #1
1586 l1=8:l2=14:l3=19:l4=22:GOSUB 4180:PRINT#2," Retour":PRINT#2," ESPACE"
1587 l1=4:l2=6:l3=22:l4=24:GOSUB 4180
1590 PEN 3:LOCATE 5,22:PRINT CHR$(233):LOCATE 5,23:PRINT CHR$(233):LOCATE 5,24:PRINT CHR$(233)
1595 LOCATE 4,23:PRINT CHR$(233):LOCATE 6,23:PRINT CHR$(233)
1600 x=5:y=23
1602 LOCATE x,y:PRINT CHR$(202)
1604 IF INKEY(72)=0 AND x=5 AND y>=23 THEN LOCATE x,y:PRINT CHR$(233):y=y-1:GOTO 1602
1606 IF INKEY(73)=0 AND x=5 AND y<=23 THEN LOCATE x,y:PRINT CHR$(233):y=y+1:GOTO 1602
1608 IF INKEY(74)=0 AND x>=5 AND y=23 THEN LOCATE x,y:PRINT CHR$(233):x=x-1:GOTO 1602
1609 IF INKEY(75)=0 AND x<=5 AND y=23 THEN LOCATE x,y:PRINT CHR$(233):x=x+1:GOTO 1602
1610 IF INKEY(47)=0 THEN WINDOW #1,1,20,17,25:CLS #1:RETURN
1611 IF INKEY(76)=0 THEN GOSUB 1620
1612 FOR h=1 TO 100:NEXT:CLEAR INPUT
1618 GOTO 1602
1620 FOR h=1 TO 100:NEXT:CLEAR INPUT
1621 IF x=4 THEN GOSUB 1630
1622 IF x=6 THEN GOSUB 1639
1624 IF y=22 THEN GOSUB 1648
1626 IF y=24 THEN GOSUB 1658
1628 IF x=5 AND y=23 THEN GOSUB 1665
1629 RETURN
1630 LOCATE x,y:PRINT CHR$(231)
1632 IF INKEY(72)=0 THEN LOCATE x,y:PRINT CHR$(240):GOSUB 2170
1634 IF INKEY(73)=0 THEN LOCATE x,y:PRINT CHR$(241):GOSUB 2110
1636 IF INKEY(74)=0 THEN LOCATE x,y:PRINT CHR$(242):GOSUB 1850
1638 IF INKEY(76)=0 THEN RETURN ELSE 1632
1639 LOCATE x,y:PRINT CHR$(231)
1640 IF INKEY(72)=0 THEN LOCATE x,y:PRINT CHR$(240):GOSUB 1910
1642 IF INKEY(73)=0 THEN LOCATE x,y:PRINT CHR$(241):GOSUB 1970
1644 IF INKEY(75)=0 THEN LOCATE x,y:PRINT CHR$(243):GOSUB 1780
1646 IF INKEY(76)=0 THEN RETURN ELSE 1640
1648 LOCATE x,y:PRINT CHR$(231)
1650 IF INKEY(74)=0 THEN LOCATE x,y:PRINT CHR$(242):GOSUB 2280
1654 IF INKEY(75)=0 THEN LOCATE x,y:PRINT CHR$(243):GOSUB 2220
1656 IF INKEY(76)=0 THEN RETURN ELSE 1650
1658 LOCATE x,y:PRINT CHR$(231)
1660 IF INKEY(74)=0 THEN LOCATE x,y:PRINT CHR$(242):GOSUB 2400
1662 IF INKEY(75)=0 THEN LOCATE x,y:PRINT CHR$(243):GOSUB 2340
1664 IF INKEY(76)=0 THEN RETURN ELSE 1660
1665 LOCATE x,y:PRINT CHR$(231)
1666 IF INKEY(72)=0 THEN LOCATE x,y:PRINT CHR$(240):GOSUB 2030
1668 IF INKEY(73)=0 THEN LOCATE x,y:PRINT CHR$(241):GOSUB 2070
1670 IF INKEY(76)=0 THEN RETURN ELSE 1666
1780 ' mvt A
1790 FOR i=1 TO 9:g(i)=a(i):NEXT
1800 a(7)=g(9):a(8)=g(6):a(9)=g(3):a(6)=g(2):a(3)=g(1):a(2)=g(4):a(1)=g(7):a(4)=g(8)
1810 t(1)=f(7):t(2)=f(8):t(3)=f(9):t(4)=d(1):t(5)=d(4):t(6)=d(7):t(7)=e(3):t(8)=e(2):t(9)=e(1):t(10)=c(9):t(11)=c(6):t(12)=c(3)
1820 f(7)=t(10):f(8)=t(11):f(9)=t(12):d(1)=t(1):d(4)=t(2):d(7)=t(3):e(3)=t(4):e(2)=t(5):e(1)=t(6):c(9)=t(7):c(6)=t(8):c(3)=t(9)
1830 GOSUB 2880
1840 RETURN
1850 FOR i=1 TO 9:g(i)=a(i):NEXT
1860 a(1)=g(3):a(2)=g(6):a(3)=g(9):a(6)=g(8):a(9)=g(7):a(8)=g(4):a(7)=g(1):a(4)=g(2)
1870 t(1)=f(7):t(2)=f(8):t(3)=f(9):t(4)=d(1):t(5)=d(4):t(6)=d(7):t(7)=e(3):t(8)=e(2):t(9)=e(1):t(10)=c(9):t(11)=c(6):t(12)=c(3)
1880 f(7)=t(4):f(8)=t(5):f(9)=t(6):d(1)=t(7):d(4)=t(8):d(7)=t(9):e(3)=t(10):e(2)=t(11):e(1)=t(12):c(9)=t(1):c(6)=t(2):c(3)=t(3)
1890 GOSUB 2880
1900 RETURN
1910 ' mvt D
1920 FOR i=1 TO 9:g(i)=d(i):NEXT
1930 d(1)=g(7):d(2)=g(4):d(3)=g(1):d(4)=g(8):d(6)=g(2):d(7)=g(9):d(8)=g(6):d(9)=g(3)
1940 t(1)=a(3):t(2)=a(6):t(3)=a(9):t(4)=e(3):t(5)=e(6):t(6)=e(9):t(7)=b(7):t(8)=b(4):t(9)=b(1):t(10)=f(3):t(11)=f(6):t(12)=f(9)
1950 a(3)=t(4):a(6)=t(5):a(9)=t(6):e(3)=t(7):e(6)=t(8):e(9)=t(9):b(7)=t(10):b(4)=t(11):b(1)=t(12):f(9)=t(3):f(6)=t(2):f(3)=t(1)
1960 GOSUB 2880:RETURN
1970 ' mvt D-
1980 FOR i=1 TO 9:g(i)=d(i):NEXT
1990 d(1)=g(3):d(2)=g(6):d(3)=g(9):d(4)=g(2):d(6)=g(8):d(7)=g(1):d(8)=g(4):d(9)=g(7)
2000 t(1)=a(3):t(2)=a(6):t(3)=a(9):t(4)=e(3):t(5)=e(6):t(6)=e(9):t(7)=b(7):t(8)=b(4):t(9)=b(1):t(10)=f(3):t(11)=f(6):t(12)=f(9)
2010 a(3)=t(10):a(6)=t(11):a(9)=t(12):e(3)=t(1):e(6)=t(2):e(9)=t(3):b(7)=t(4):b(4)=t(5):b(1)=t(6):f(9)=t(9):f(6)=t(8):f(3)=t(7)
2020 GOSUB 2880:RETURN
2030 ' mvt T
2040 t(1)=a(2):t(2)=a(5):t(3)=a(8):t(4)=e(2):t(5)=e(5):t(6)=e(8):t(7)=b(8):t(8)=b(5):t(9)=b(2):t(10)=f(2):t(11)=f(5):t(12)=f(8)
2050 a(2)=t(4):a(5)=t(5):a(8)=t(6):e(2)=t(7):e(5)=t(8):e(8)=t(9):b(8)=t(10):b(5)=t(11):b(2)=t(12):f(2)=t(1):f(5)=t(2):f(8)=t(3)
2060 GOSUB 2880:RETURN
2070 ' mvt T-
2080 t(1)=a(2):t(2)=a(5):t(3)=a(8):t(4)=e(2):t(5)=e(5):t(6)=e(8):t(7)=b(8):t(8)=b(5):t(9)=b(2):t(10)=f(2):t(11)=f(5):t(12)=f(8)
2090 a(2)=t(10):a(5)=t(11):a(8)=t(12):e(2)=t(1):e(5)=t(2):e(8)=t(3):b(8)=t(4):b(5)=t(5):b(2)=t(6):f(2)=t(7):f(5)=t(8):f(8)=t(9)
2100 GOSUB 2880:RETURN
2110 ' mvt G
2120 FOR i=1 TO 9:g(i)=c(i):NEXT
2130 c(1)=g(7):c(2)=g(4):c(3)=g(1):c(4)=g(8):c(6)=g(2):c(7)=g(9):c(8)=g(6):c(9)=g(3)
2140 t(1)=a(1):t(2)=a(4):t(3)=a(7):t(4)=e(1):t(5)=e(4):t(6)=e(7):t(7)=b(9):t(8)=b(6):t(9)=b(3):t(10)=f(1):t(11)=f(4):t(12)=f(7)
2150 a(1)=t(10):a(4)=t(11):a(7)=t(12):e(1)=t(1):e(4)=t(2):e(7)=t(3):b(9)=t(4):b(6)=t(5):b(3)=t(6):f(1)=t(7):f(4)=t(8):f(7)=t(9)
2160 GOSUB 2880:RETURN
2170 FOR i=1 TO 9:g(i)=c(i):NEXT
2180 c(1)=g(3):c(2)=g(6):c(3)=g(9):c(4)=g(2):c(6)=g(8):c(7)=g(1):c(8)=g(4):c(9)=g(7)
2190 t(1)=a(1):t(2)=a(4):t(3)=a(7):t(4)=e(1):t(5)=e(4):t(6)=e(7):t(7)=b(9):t(8)=b(6):t(9)=b(3):t(10)=f(1):t(11)=f(4):t(12)=f(7)
2200 a(1)=t(4):a(4)=t(5):a(7)=t(6):e(1)=t(7):e(4)=t(8):e(7)=t(9):b(9)=t(10):b(6)=t(11):b(3)=t(12):f(1)=t(1):f(4)=t(2):f(7)=t(3)
2210 GOSUB 2880:RETURN
2220 ' mvt H-
2230 FOR i=1 TO 9:g(i)=f(i):NEXT
2240 f(1)=g(3):f(2)=g(6):f(3)=g(9):f(4)=g(2):f(6)=g(8):f(7)=g(1):f(8)=g(4):f(9)=g(7)
2250 t(1)=a(1):t(2)=a(2):t(3)=a(3):t(4)=d(1):t(5)=d(2):t(6)=d(3):t(7)=b(1):t(8)=b(2):t(9)=b(3):t(10)=c(1):t(11)=c(2):t(12)=c(3)
2260 a(1)=t(10):a(2)=t(11):a(3)=t(12):d(1)=t(1):d(2)=t(2):d(3)=t(3):b(1)=t(4):b(2)=t(5):b(3)=t(6):c(1)=t(7):c(2)=t(8):c(3)=t(9)
2270 GOSUB 2880:RETURN
2280 ' mvt H
2290 FOR i=1 TO 9:g(i)=f(i):NEXT
2300 f(1)=g(7):f(2)=g(4):f(3)=g(1):f(4)=g(8):f(6)=g(2):f(7)=g(9):f(8)=g(6):f(9)=g(3)
2310 t(1)=a(1):t(2)=a(2):t(3)=a(3):t(4)=d(1):t(5)=d(2):t(6)=d(3):t(7)=b(1):t(8)=b(2):t(9)=b(3):t(10)=c(1):t(11)=c(2):t(12)=c(3)
2320 a(1)=t(4):a(2)=t(5):a(3)=t(6):d(1)=t(7):d(2)=t(8):d(3)=t(9):b(1)=t(10):b(2)=t(11):b(3)=t(12):c(1)=t(1):c(2)=t(2):c(3)=t(3)
2330 GOSUB 2880:RETURN
2340 ' mvt B
2350 FOR i=1 TO 9:g(i)=e(i):NEXT
2360 e(1)=g(7):e(2)=g(4):e(3)=g(1):e(4)=g(8):e(6)=g(2):e(7)=g(9):e(8)=g(6):e(9)=g(3)
2370 t(1)=a(7):t(2)=a(8):t(3)=a(9):t(4)=d(7):t(5)=d(8):t(6)=d(9):t(7)=b(7):t(8)=b(8):t(9)=b(9):t(10)=c(7):t(11)=c(8):t(12)=c(9)
2380 a(7)=t(10):a(8)=t(11):a(9)=t(12):d(7)=t(1):d(8)=t(2):d(9)=t(3):b(7)=t(4):b(8)=t(5):b(9)=t(6):c(7)=t(7):c(8)=t(8):c(9)=t(9)
2390 GOSUB 2880:RETURN
2400 ' mvt B-
2410 FOR i=1 TO 9:g(i)=e(i):NEXT
2420 e(1)=g(3):e(2)=g(6):e(3)=g(9):e(4)=g(2):e(6)=g(8):e(7)=g(1):e(8)=g(4):e(9)=g(7)
2430 t(1)=a(7):t(2)=a(8):t(3)=a(9):t(4)=d(7):t(5)=d(8):t(6)=d(9):t(7)=b(7):t(8)=b(8):t(9)=b(9):t(10)=c(7):t(11)=c(8):t(12)=c(9)
2440 a(7)=t(4):a(8)=t(5):a(9)=t(6):d(7)=t(7):d(8)=t(8):d(9)=t(9):b(7)=t(10):b(8)=t(11):b(9)=t(12):c(7)=t(1):c(8)=t(2):c(9)=t(3)
2450 GOSUB 2880:RETURN
2460 '#############################################
2470 '######## DESSIN DU CUBE EN 2D ET 3D #########
2480 '#############################################
2490 INK 1,0
2500 INK 1,0 'en 2d
2510 FOR y=160 TO 380 STEP 20
2520 PLOT 100,y:DRAW 160,y,1
2530 NEXT
2540 FOR x=20 TO 260 STEP 80
2550 FOR y=240 TO 300 STEP 20
2560 PLOT x,y:DRAW x+60,y
2570 NEXT:NEXT
2580 FOR x=20 TO 320 STEP 20
2590 PLOT x,240:DRAW x,300
2600 NEXT
2610 FOR y=160 TO 320 STEP 80
2620 FOR x=100 TO 160 STEP 20
2630 PLOT x,y:DRAW x,y+60
2640 NEXT:NEXT
2650 INK 1,0 'en 3d
2660 PLOT 372,324:DRAW 480,380,1
2670 PLOT 372,286:DRAW 480,340
2680 PLOT 372,246:DRAW 480,300
2690 PLOT 372,204:DRAW 480,260
2700 PLOT 408,186:DRAW 518,240
2710 PLOT 444,168:DRAW 554,223
2720 PLOT 480,154:DRAW 588,204
2730 PLOT 372,324:DRAW 372,204
2740 PLOT 406,341:DRAW 406,223
2750 PLOT 444,360:DRAW 444,240
2760 PLOT 480,380:DRAW 480,260
2770 PLOT 518,360:DRAW 518,240
2780 PLOT 554,341:DRAW 554,223
2790 PLOT 588,324:DRAW 588,204
2800 PLOT 480,380:DRAW 588,324
2810 PLOT 480,340:DRAW 588,286
2820 PLOT 480,300:DRAW 588,246
2830 PLOT 480,260:DRAW 588,204
2840 PLOT 444,240:DRAW 554,186
2850 PLOT 406,223:DRAW 518,168
2860 PLOT 372,204:DRAW 480,154
2870 RETURN
2880 '######################################
2890 '######### COLORIAGE DES CASES ########
2900 '######################################
2910 MOVE 110,290:FILL a(1):MOVE 130,290:FILL a(2):MOVE 150,290:FILL a(3):MOVE 110,270:FILL a(4):MOVE 130,270:FILL a(5):MOVE 150,270:FILL a(6):MOVE 110,250:FILL a(7):MOVE 130,250:FILL a(8):MOVE 150,250:FILL a(9)
2920 MOVE 270,290:FILL b(1):MOVE 290,290:FILL b(2):MOVE 310,290:FILL b(3):MOVE 270,270:FILL b(4):MOVE 290,270:FILL b(5):MOVE 310,270:FILL b(6):MOVE 270,250:FILL b(7):MOVE 290,250:FILL b(8):MOVE 310,250:FILL b(9)
2930 MOVE 30,290:FILL c(1):MOVE 50,290:FILL c(2):MOVE 70,290:FILL c(3):MOVE 30,270:FILL c(4):MOVE 50,270:FILL c(5):MOVE 70,270:FILL c(6):MOVE 30,250:FILL c(7):MOVE 50,250:FILL c(8):MOVE 70,250:FILL c(9)
2940 MOVE 190,290:FILL d(1):MOVE 210,290:FILL d(2):MOVE 230,290:FILL d(3):MOVE 190,270:FILL d(4):MOVE 210,270:FILL d(5):MOVE 230,270:FILL d(6):MOVE 190,250:FILL d(7):MOVE 210,250:FILL d(8):MOVE 230,250:FILL d(9)
2950 MOVE 110,210:FILL e(1):MOVE 130,210:FILL e(2):MOVE 150,210:FILL e(3):MOVE 110,190:FILL e(4):MOVE 130,190:FILL e(5):MOVE 150,190:FILL e(6):MOVE 110,170:FILL e(7):MOVE 130,170:FILL e(8):MOVE 150,170:FILL e(9)
2960 MOVE 110,370:FILL f(1):MOVE 130,370:FILL f(2):MOVE 150,370:FILL f(3):MOVE 110,350:FILL f(4):MOVE 130,350:FILL f(5):MOVE 150,350:FILL f(6):MOVE 110,330:FILL f(7):MOVE 130,330:FILL f(8):MOVE 150,330:FILL f(9)
2970 MOVE 380,324:FILL a(1):MOVE 420,340:FILL a(2):MOVE 450,360:FILL a(3):MOVE 380,286:FILL a(4):MOVE 420,286:FILL a(5):MOVE 450,300:FILL a(6):MOVE 380,240:FILL a(7):MOVE 420,246:FILL a(8):MOVE 450,260:FILL a(9)
2980 MOVE 490,340:FILL d(1):MOVE 530,324:FILL d(2):MOVE 570,300:FILL d(3):MOVE 490,300:FILL d(4):MOVE 530,286:FILL d(5):MOVE 570,260:FILL d(6):MOVE 490,260:FILL d(7):MOVE 530,240:FILL d(8):MOVE 570,223:FILL d(9)
2990 MOVE 406,204:FILL e(1):MOVE 444,223:FILL e(2):MOVE 480,240:FILL e(3):MOVE 444,184:FILL e(4):MOVE 480,204:FILL e(5):MOVE 518,223:FILL e(6):MOVE 480,168:FILL e(7):MOVE 518,184:FILL e(8):MOVE 554,204:FILL e(9)
3000 RETURN
3010 '#################################
3020 '############ MELANGE ############
3030 '#################################
3040 l1=10:l2=16:l3=19:l4=23
3050 ti$="MELANGE":t=2:DIM fe$(t)
3060 fe$(1)=" Oui   ":fe$(2)=" Non   "
3065 mo=4:GOSUB 4000:WINDOW #1,1,20,17,25:CLS #1
3070 ON j-2 GOTO 3080,600
3080 FOR QWi=0 TO 12
3090 er=INT(RND(1)*12)+1:GOSUB 3100:NEXT
3100 ON er GOSUB 1780,1850,1910,1970,2030,2070,2110,2170,2220,2280,2340,2400
3110 RETURN
3250 '############################
3260 '###### MODE D'EMPLOI #######
3270 '############################
3280 MODE 2
3290 INK 1,0:INK 0,26:PEN 1
3300 PRINT:PRINT:PRINT"          ----  M O D E   D ' E M F L O I  ----"
3310 PRINT:PRINT:PRINT
3320 PRINT"    RUBICK est une reproduction de ce jeu celebre sur ordinateur.L'avantage est que 1'ordinateur vous permet de voir toutes les vues d'un seul coup; de plus il vous donne les derniers coups joues ainsi qu'une vue en 3 dimensions."
3330 PRINT"Tous les mouvements et les formu1es se font a partir de la vue de face. Cette   vue correspond a 1'intersection des six faces en vue plane et a la vue de gauche de la vue en trois dimensions."
3340 PRINT"Vous pouvez le melanger automatiquement ou manue11ement."
3350 PRINT "Les mouvements notes par des lettres (telles que: A+,A-,B+,B-,...) vous sont donnes avec les touches correspondantes."
3360 PRINT:PRINT"Bien sur ce jeu necessite un certain temps d'adaptation (pour 1es mouvements et les formu1es notamment),mais n'ayez crainte,ce n'est pas la mer a boire."
3370 PRINT:PRINT:PRINT"       Alors les nostalgiques du rubik's,a vos c1aviers..."
3380 PRINT:PRINT:PRINT"                                    appuyez sur une touche !":CALL &BB06
3390 RETURN
4000                             ' GENERATEUR DE MENUS DEROULANTS
4010 op=1:GOSUB 4180
4015 PEN 1
4020 PRINT#2,ti$
4030 PRINT#2
4040 FOR j=1 TO t
4050 LOCATE #2,1,j+2:PRINT#2,fe$(j)
4060 NEXT
4070 j=3
4080 GOSUB 4090:GOTO 4110
4090 PEN #2,3:LOCATE #2,1,j:PRINT#2,CHR$(24)fe$(j-2)CHR$(24):RETURN
4100 PEN #2,1:LOCATE #2,1,j:PRINT#2,fe$(j-2):RETURN
4110 IF (INKEY(73)=0 OR INKEY(2)=0) AND j-2<t THEN GOSUB 4100:j=j+1:GOSUB 4090
4120 IF (INKEY(72)=0 OR INKEY(0)=0) AND j>3 THEN GOSUB 4100:j=j-1:GOSUB 4090
4130 IF INKEY(76)=0 OR INKEY(9)=0 THEN 4160
4140 FOR h=1 TO 100:NEXT:CLEAR INPUT
4150 GOTO 4110
4160 CLEAR INPUT
4170 ERASE fe$:RETURN
4180 WINDOW #2,l1-1,l2+1,l3-1,l4+1:PEN #2,1:CLS #2
4190 WINDOW #2,l1,l2,l3,l4:PEN #2,1:CLS #2
4200 PLOT (l1-2)*8*mo-xo,400-(l4+1)*16-yo,1,0:DRAW (l2+1)*8*mo-xo,400-(l4+1)*16-yo:DRAW (l2+1)*8*mo-xo,400-(l3-2)*16-yo:DRAW (l1-2)*8*mo-xo,400-(l3-2)*16-yo:DRAW (l1-2)*8*mo-xo,400-(l4+1)*16-yo
4210 IF op=0 THEN RETURN
4220 PLOT (l1-2)*8*mo-xo+4,400-(l4+1)*16-yo+8,1,0:DRAW (l2+1)*8*mo-xo-4,400-(l4+1)*16-yo+8:DRAW (l2+1)*8*mo-xo-4,400-(l3-1)*16-yo-8:DRAW (l1-2)*8*mo-xo+4,400-(l3-1)*16-yo-8
4230 DRAW (l1-2)*8*mo-xo+4,400-(l4+1)*16-yo+8:op=0:RETURN
